import utils from 'util';
import * as utils_one from 'test-utils';

const params = { password: 'df345w43jkj3443d' };

const myFuncPromise = utils.promisify(utils_one.runMePlease);

try {
    const result = await myFuncPromise(params);
    console.log(result);
} catch (error) {
    console.log(error);
}